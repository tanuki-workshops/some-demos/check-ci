# check-ci

## Ajouter un nouveau cluster
> à faire en avance

- Vérifier si il existe déjà une configuration K8S
  - Si oui la supprimer
  - Vérifier si les **Shared Runners** sont activés (si non les activer)
  - Supprimer le **Kubernetes executor** s'il existe
- Copier le token de CI du groupe
- Ouvrir le projet de Cluster Management avec GitPod
  - Vérifier que les **Shared Runners** sont activés pour ce projet (si non les activer)
  - Copier le token dans `./applications/gitlab-runner/values.yaml`
  - Lancer `./01-create-cluster.sh` et attendre
  - Lancer `./02-integration-preamble.sh`
- Aller dans le menu **Kubernetes** du groupe
  - Faire l'intégration manuellement
  - Choisir le "base domain" `<ip>.nip.io`
  - Sauver
  - Aller dans **Integrations** et activer l'intégration  Prometheus + Sauver
  - Dans **Advanced Settings**, sélectionner le "Cluster management project" + Sauver
- Retourner dans GitPod
  - **Commit** + **Push** => déclenchement du pipeline (sinon le forcer manuellement)
  - Vérifier avec K9S que tout va bien
- Aller dans les paramètres de CI du Groupe
  - Vérifier que le runner kube a bien été créé
  - Editer le runner pour cocher **"Run untagged jobs"** + Sauver
  - Désactiver les **Shared Runners**
- Utiliser le présent projet pour vérifier que tout se lance correctement
  - Lancer le pipeline pour vérifier que le Kubernetes Executor est bien utilisé
